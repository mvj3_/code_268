UIWebView *_webView;
NSString *userAgent;
 
- (void)createHttpRequest {
	_webView = [[UIWebView alloc] init];
	_webView.delegate = self;
	[_webView loadRequest:[NSURLRequest requestWithURL:
	[NSURL URLWithString:@"http://www.eoe.cn"]]];
	NSLog(@"%@", [self userAgentString]);
	[_webView release];
}

-(NSString *)userAgentString
{
	while (self.userAgent == nil) 
	{
		NSLog(@"%@", @"in while");
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		}
	return self.userAgent;
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
	if (webView == _webView) {
	self.userAgent = [request valueForHTTPHeaderField:@"User-Agent"];
	// Return no, we don't care about executing an actual request.
	return NO;
}
	return YES;
}

- (void)dealloc {
[userAgent release];
    [super dealloc];
}